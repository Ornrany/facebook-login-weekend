package com.example.rany.socialloginweekend;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private CallbackManager manager;
    private static final String EMAIL = "email";
    private LoginButton loginButton;
    public static final String TAG = "oooo";
    private AccessToken accessToken;
    private  boolean isLoggedIn;
    private Button btnShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        fbLogin();

        accessToken = AccessToken.getCurrentAccessToken();
        isLoggedIn = accessToken != null && !accessToken.isExpired();
        Log.e(TAG, "Login status "+ isLoggedIn);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog dialog = new ShareDialog(MainActivity.this);
                // ----------- Share link -------------------------
//                if(dialog.canShow(ShareLinkContent.class)){
//                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
////                            .setContentUrl(Uri.parse(name.getText().toString))
//                            .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
//                            .setShareHashtag(new ShareHashtag.Builder()
//                                    .setHashtag("#"+ "FacebookIntergration").build())
//                            .setQuote("Useful")
//                            .build();
//                    dialog.show(linkContent);
//            }
                // ************ End share link***************

                // --------------Share Image --------------
                if(dialog.canShow(SharePhotoContent.class)){
                    // convert image to bitmap file
                    Bitmap bitmap =
                            BitmapFactory.decodeResource(getResources(), R.drawable.sunset);

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .build();

                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();
                    dialog.show(content);
                }

            }
        });

    }

    private void getUserInfo(AccessToken token){
        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            String username = object.getString("name");
                            String id = object.getString("id");
                            String email = object.getString("email");
                            String profile = "https://graph.facebook.com/"+
                                    object.getString("id")+"/picture?type=large";
                            Log.e(TAG, "onCompleted: "+ username + " ," +
                                    email + " , " + profile);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Application code
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void fbLogin(){
        loginButton.registerCallback(manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "onSuccess: ");
                getUserInfo(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "onCancel: " );
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "onError: "+ error.getMessage());
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        manager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView() {
        manager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);
        btnShare = findViewById(R.id.btnShareImage);
    }
}
